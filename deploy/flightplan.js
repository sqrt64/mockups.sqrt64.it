// flightplan.js
var plan = require('flightplan')

// configuration
// edit this object to match your requirements!
const config = {
  deployHost: 'mockups.sqrt64.it',
  deployPath: '/home/otto/www/mockupsapp',
  deployUser: 'otto'
}

const configStaging = {
  deployHost: 'staging.mockups.sqrt64.it',
  deployPath: '/home/otto/www/mockupsapp',
  deployUser: 'otto'
}

// do not edit under this line
plan.target('production', {
  host: config.deployHost,
  path: config.deployPath,
  username: config.deployUser,
  agent: process.env.SSH_AUTH_SOCK,
  privateKey: '/home/abidibo/.ssh/id_rsa'
})

plan.target('staging', {
  host: configStaging.deployHost,
  path: configStaging.deployPath,
  username: configStaging.deployUser,
  agent: process.env.SSH_AUTH_SOCK,
  privateKey: '/home/abidibo/.ssh/id_rsa'
})

let tmpDir = 'mockupsapp-' + new Date().getTime()

// run commands on localhost
plan.local(function (local) {
  local.log('Run build')
  local.exec('yarn build')

  local.log('Copy files to remote host')
  var filesToCopy = local.exec('ls -d ../build/* ../build/*/*/*', {silent: true})
  // rsync files to all the target's remote hosts
  local.with('cd ..', () => {
    local.transfer(filesToCopy, '/tmp/' + tmpDir)
  })
})

// run commands on the target's remote hosts
plan.remote(function (remote) {
  let c = remote._context.remote
  remote.log('Remove previous')
  remote.exec(`rm -r ${c.path}/previous`)
  remote.log('Move current to previous')
  remote.exec(`mv ${c.path}/current ${c.path}/previous`)
  remote.log('Move files to current')
  remote.exec('cp -R /tmp/' + tmpDir + `/build ${c.path}/current`)
  remote.rm('-rf /tmp/' + tmpDir)
})
