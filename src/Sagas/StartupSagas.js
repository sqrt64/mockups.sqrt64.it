import { put } from 'redux-saga/effects'
import AuthActions from '../Redux/Auth'

// process STARTUP actions
export function * startup (action) {
  console.log('STARTUP', 'whoami')
  yield put(AuthActions.whoamiRequest())
}
